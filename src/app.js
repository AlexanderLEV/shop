import ng from 'angular';
import 'angular-ui-router';
import 'bootstrap/less/bootstrap.less';
import NgComponents from './components';

ng.module('shop', ['ui.router', NgComponents])
  .config(($locationProvider, $stateProvider) => {
    'ngInject';
    $locationProvider.html5Mode(true);
    const states = [
      {
        name: 'home',
        url: '/',
        component: 'categoryList'
      },
      {
        name: 'registration',
        url: '/registration',
        component: 'registration'
      },
      {
        name: 'productDescription',
        url: '/product/:id',
        component: 'productDescription'
      },
      {
        name: 'productList',
        url: '/products/:id',
        component: 'productList',
      }
    ];
    states.forEach(state => $stateProvider.state(state));
  });
