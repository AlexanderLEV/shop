import template from './categoryItem.html';

export default {
  template,
  bindings: {
    category: '<',
    getCategories: '&'
  }
};
