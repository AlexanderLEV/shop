import ng from 'angular';
import CategoryItemComponent from './categoryItem.component';

export default ng.module('categoryItem.components.app', [])
  .component('categoryItem', CategoryItemComponent)
  .name;
