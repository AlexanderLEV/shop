/* global API_URL */

export default class CategoryService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  list() {
    return this.$http.get(`${API_URL}/`).then(result => result.data);
  }
  getCategories(id) {
    return this.$http.get(`${API_URL}/category/${id}`).then(result => result.data);
  }
}
