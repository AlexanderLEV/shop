import template from './categoryList.html';
import controller from './categoryList.controller';

export default {
  template,
  controller,
  bindings: {
    getProducts: '<'
  }
};
