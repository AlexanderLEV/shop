export default class CategoryListController {
  constructor($rootScope, $state, CategoryService, ProductService) {
    'ngInject';
    this.CategoryService = CategoryService;
    this.ProductService = ProductService;
    this.$state = $state;
    $rootScope.header = 'Home';
  }

  $onInit() {
    this.init();
  }

  init() {
    this.CategoryService.list().then((categories) => {
      this.categories = categories;
    });
  }

  getCategories(id) {
    this.CategoryService.getCategories(id).then((result) => {
      if (result.length !== 0) {
        this.categories = result;
      } else {
        this.$state.go('productList', { id });
      }
    });
  }
}
