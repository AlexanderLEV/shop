import ng from 'angular';
import CategoryListComponent from './categoryList.component';
import CategoryService from './category.service';

export default ng.module('categoryList.components.app', [])
  .component('categoryList', CategoryListComponent)
  .service('CategoryService', CategoryService)
  .name;
