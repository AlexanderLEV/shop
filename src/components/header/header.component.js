import template from './header.html';
import styles from '../../assets/styles/index.less';

export default {
  template,
  styles
};
