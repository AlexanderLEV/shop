import ng from 'angular';
import HeaderComponent from './header.component';

export default ng.module('header.components.app', [])
  .component('shopHeader', HeaderComponent)
  .name;
