import 'sweetalert/dist/sweetalert.min.js';
import 'sweetalert/dist/sweetalert.css';
import 'sweetalert/themes/google/google.css';

import ng from 'angular';
import Header from './header';
import Registration from './registration';
import CategoryList from './categoryList';
import CategoryItem from './categoryItem';
import ProductItem from './productItem';
import ProductList from './productList';
import ProductDescription from './productDescription';

export default ng.module('app.components', [Header, CategoryList, CategoryItem, ProductList, ProductItem,
                                            Registration, ProductDescription])
                                            .name;
