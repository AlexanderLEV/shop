import ng from 'angular';
import ProductDescriptionComponent from './productDescription.component';

export default ng.module('productDescription.components.app', [])
  .component('productDescription', ProductDescriptionComponent)
  .name;
