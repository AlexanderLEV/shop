import template from './productDescription.html';
import controller from './productDescription.controller';

export default {
  template,
  controller,
  bindings: {
    product: '<'
  }
};
