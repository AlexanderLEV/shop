export default class ProductDescriptionController {
  constructor($rootScope, $stateParams, ProductService) {
    'ngInject';
    this.ProductService = ProductService;
    this.id = $stateParams.id;

    // Document title
    $rootScope.header = 'Description';
  }

  $onInit() {
    this.init();
  }

  init() {
    this.ProductService.description(this.id).then((product) => { this.product = product; });
  }
}
