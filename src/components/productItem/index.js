import ng from 'angular';
import ProductItemComponent from './productItem.component';

export default ng.module('productItem.components.app', [])
  .component('productItem', ProductItemComponent)
  .name;

