import template from './productItem.html';

export default {
  template,
  bindings: {
    product: '<'
  }
};
