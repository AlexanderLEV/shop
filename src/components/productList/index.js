import ng from 'angular';
import ProductListComponent from './productList.component';
import ProductService from './product.service';

export default ng.module('productList.components.app', [])
  .component('productList', ProductListComponent)
  .service('ProductService', ProductService)
  .name;
