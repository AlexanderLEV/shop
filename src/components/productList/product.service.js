/* global API_URL */

export default class ProductService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  list() {
    return this.$http.get(`${API_URL}/products`).then(result => result.data);
  }

  detail(id) {
    return this.$http.get(`${API_URL}/products/${id}`).then(result => result.data);
  }

  description(id) {
    return this.$http.get(`${API_URL}/products/product/${id}`).then(result => result.data);
  }
}
