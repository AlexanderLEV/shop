import template from './productList.html';
import controller from './productList.controller';

export default {
  template,
  controller,
  bindings: {
    getProducts: '<',
  }
};
