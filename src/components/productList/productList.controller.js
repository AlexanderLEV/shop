export default class ProductListController {
  constructor($rootScope, $stateParams, ProductService) {
    'ngInject';
    this.ProductService = ProductService;
    this.id = $stateParams.id;

    // Document title
    $rootScope.header = 'Products';
  }

  $onInit() {
    this.init();
  }

  init() {
    this.ProductService.detail(this.id).then((products) => {
      this.products = products;
    });
  }
}
