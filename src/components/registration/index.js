import ng from 'angular';
import RegistrationComponent from './registration.component';
import UserService from './user.service';

export default ng.module('registration.components.app', [])
  .component('registration', RegistrationComponent)
  .service('UserService', UserService)
  .name;
