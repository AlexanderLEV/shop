/* global swal */

export default class RegistrationController {
  constructor($rootScope, $state, UserService) {
    'ngInject';
    this.UserService = UserService;
    this.$state = $state;

    // Document title
    $rootScope.header = 'Registration';
  }

  toRegister() {
    const user = {
      name: this.name,
      email: this.email,
      password: this.password
    };

    try {
      this.UserService.add(user).then((res) => {
        swal({ title: 'Welcome', text: `${res.user.name}, Welcome!`, timer: 1300, showConfirmButton: false });
        return res;
      }).then(this.$state.go('home'));
    } catch (ex) {
      swal({ title: 'Error', text: `${ex.message}`, timer: 1300, showConfirmButton: false });
    }
  }
}
