/* global API_URL */

export default class UserService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  add(user) {
    for (const value in user) {             // TODO: ForInStatement is not allowed.
      if (UserService.isEmpty(user[value])) {
        throw new Error(`${value} cant be is empty!`);
      }
    }

    return this.$http.post(`${API_URL}/users`, user).then(result => result.data);
  }

  static isEmpty(str) {
    return !str;
  }
}

